import calcInvast from "./calculate1.js"

const navIndecator = document.querySelector(".nav-menu-indecator img");

navIndecator.addEventListener("click", () => {
    const isActive = navIndecator.dataset.status === "close" ? false : true;
    const imgLink = { open: "/image/borger-open.svg", close: "/image/burger-close.svg" };
    if (!isActive) {
        navIndecator.src = imgLink.open;
        navIndecator.dataset.status = "open";
        document.querySelector(".header .nav").classList.add("height-auto")
    } else {
        navIndecator.src = imgLink.close;
        navIndecator.dataset.status = "close";
        document.querySelector(".header .nav").classList.remove("height-auto")
    }
})

const langIndecator = document.querySelector(".lang-menu-indecator span");


langIndecator.addEventListener("click", () => {
    const isActive = langIndecator.dataset.status === "close" ? false : true;
    if (!isActive) {
        langIndecator.dataset.status = "open"
        document.querySelector(".header .lang").classList.add("height-auto");
    } else {
        langIndecator.dataset.status = "close"
        document.querySelector(".header .lang").classList.remove("height-auto")
    }
})

/*
const cardBlock = document.querySelector("#scroll-menu");
const scrollHeader = document.querySelector(".container");
let coodrsScroll = 0;
let flagS = false;
cardBlock.scrollWidth
console.dir(scrollHeader.clientWidth)
console.dir(cardBlock.scrollWidth)
const divScroll = (cardBlock.scrollWidth / scrollHeader.clientWidth) - 1;

setInterval(() => {
    if (-coodrsScroll / scrollHeader.clientWidth  >= divScroll) {
        coodrsScroll = 350
    } else if((-coodrsScroll / scrollHeader.clientWidth)  < divScroll ) {
        coodrsScroll = coodrsScroll - 10
        cardBlock.style.transform = `translateX(${coodrsScroll}px)`;
    }
}, 100)
*/
/*
const slider = document.querySelector("#scroll-menu");
const container = document.querySelector("body");
let [...sliderElements] = slider.children;
sliderElements.forEach((el)=>{
    el.style.left = 0 + "px"
})

const scrollElement = -slider.scrollWidth





const intervalTest = setInterval(() => {
   sliderElements.forEach((el)=>{

    console.log();

    if(scrollElement > parseFloat(el.style.left) - container.clientWidth){
        el.style.left = 150 + "px"
    }
    let cood = parseInt(el.style.left) - 100
       el.style.left = cood + "px"
   })
}, 1000)
*/


 /*
    console.log(slider.scrollWidth / sliderElements.length); // 2220 = 317.14;
    const width1 = slider.scrollWidth / sliderElements.length;
    sliderElements.forEach((e, i) => {
       
    })
    */

const slider = document.querySelector("#scroll-menu");
const container = document.querySelector("body");
let [...sliderElements] = slider.children;
sliderElements.forEach((el)=>{
    el.style.left = 0 + "px"
})

const scrollElement = -slider.scrollWidth





const intervalTest = setInterval(() => {
   sliderElements.forEach((el)=>{


    if(scrollElement > parseFloat(el.style.left) - container.clientWidth){
        el.style.left = 150 + "px"
    }
    let cood = parseInt(el.style.left) - 100
       el.style.left = cood + "px"
   })
}, 1000)

const sumElement = document.querySelector(".input-calc input");
const tbody = document.getElementById("table-invest");

sumElement.addEventListener("input", (e) => {
    if(/^[0-9.]{2,7}$/.test(e.target.value)){
        calcInvast(parseFloat(e.target.value), tbody); 
    }
})



window.addEventListener("contextmenu", () => {
    console.log("Відкрита консоль");
})


